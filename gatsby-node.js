const _ = require(`lodash`)
const Promise = require(`bluebird`)
const path = require(`path`)
const slash = require(`slash`)
const createPaginatedPages = require('gatsby-paginate')
// Implement the Gatsby API “createPages”. This is
// called after the Gatsby bootstrap is finished so you have
// access to any information necessary to programmatically
// create pages.
// Will create pages for Wordpress pages (route : /{slug})
// Will create pages for Wordpress posts (route : /post/{slug})

const similarPosts = (items, length) => {
  let similar = []
  let previous = { node: { slug: '' } }
  const hasDuplicate = (array, item) =>
    array.filter(({ node }) => node.slug === item.node.slug).length > 0

  for (let index = 0; index < length; index++) {
    let item = items[Math.floor(Math.random() * items.length)]
    if (hasDuplicate(similar, item)) {
      item = items[Math.floor(Math.random() * items.length)]
    }
    similar.push(item)
  }
  return similar
}

const similarPostsByFunction = (items, func, length) => {
  let similar = []
  let previous = { node: { slug: '' } }
  const hasDuplicate = (array, item) =>
    array.filter(({ node }) => node.slug === item.node.slug).length > 0

  for (let index = 0; index < length; index++) {
    let item = items[Math.floor(Math.random() * items.length)]
    if (hasDuplicate(similar, item)) {
      item = items[Math.floor(Math.random() * items.length)]
    }
    similar.push(item)
  }
  return similar
}

exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators
  return new Promise((resolve, reject) => {
    // The “graphql” function allows us to run arbitrary
    // queries against the local Wordpress graphql schema. Think of
    // it like the site has a built-in database constructed
    // from the fetched data that you can run queries against.

    // ==== PAGES (WORDPRESS NATIVE) ====
    graphql(
      `
        {
          allWordpressPage {
            edges {
              node {
                id
                slug
                status
                template
              }
            }
          }
        }
      `
    )
      .then(result => {
        if (result.errors) {
          console.log(result.errors)
          reject(result.errors)
        }

        // Create Page pages.
        const pageTemplate = path.resolve(`./src/templates/page.js`)
        // We want to create a detailed page for each
        // page node. We'll just use the Wordpress Slug for the slug.
        // The Page ID is prefixed with 'PAGE_'
        _.each(result.data.allWordpressPage.edges, edge => {
          // Gatsby uses Redux to manage its internal state.
          // Plugins and sites can use functions like "createPage"
          // to interact with Gatsby.
          createPage({
            // Each page is required to have a `path` as well
            // as a template component. The `context` is
            // optional but is often necessary so the template
            // can query data specific to each page.
            path: `/page/${edge.node.slug}/`,
            component: slash(pageTemplate),
            context: {
              id: edge.node.id,
            },
          })
        })
      })
      // ==== END PAGES ====

      // ==== POSTS (WORDPRESS NATIVE AND ACF) ====
      .then(() => {
        graphql(
          `
            {
              allWordpressPost {
                edges {
                  node {
                    id
                    slug
                    status
                    template
                    format
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }
          const postTemplate = path.resolve(`./src/templates/post.js`)
          // We want to create a detailed page for each
          // post node. We'll just use the Wordpress Slug for the slug.
          // The Post ID is prefixed with 'POST_'
          _.each(result.data.allWordpressPost.edges, edge => {
            createPage({
              path: `/post/${edge.node.slug}/`,
              component: slash(postTemplate),
              context: {
                id: edge.node.id,
              },
            })
          })
          resolve()
        })
      })
      // ==== END POSTS ====

      // ==== PROJECTS (WORDPRESS NATIVE AND ACF) ====
      .then(() => {
        graphql(
          `
            {
              allWordpressWpProjects {
                edges {
                  node {
                    title
                    id
                    slug
                    _function
                    _place
                    _status
                    _year
                    featured_media {
                      localFile {
                        childImageSharp {
                          sizes(maxWidth: 680) {
                            base64
                            aspectRatio
                            src
                            srcSet
                            sizes
                          }
                        }
                      }
                    }
                    wpml_translations {
                      locale
                      wordpress_id
                      post_title
                      post_content
                      href
                      acf {
                        description
                      }
                    }
                    acf {
                      description
                    }
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }
          const projectTemplate = path.resolve(`./src/templates/project.js`)

          // We want to create a detailed page for each
          // post node. We'll just use the Wordpress Slug for the slug.
          // The Post ID is prefixed with 'POST_'

          createPaginatedPages({
            edges: result.data.allWordpressWpProjects.edges,
            createPage: createPage,
            pageTemplate: './src/templates/projects.js',
            pageLength: 21, // This is optional and defaults to 10 if not used
            pathPrefix: 'projects', // This is optional and defaults to an empty string if not used
          })

          // createPaginatedPages({
          //   edges: result.data.allWordpressWpProjects.edges,
          //   createPage: createPage,
          //   pageTemplate: './src/templates/projects.js',
          //   pageLength: 21, // This is optional and defaults to 10 if not used
          //   pathPrefix: `en/projects`, // This is optional and defaults to an empty string if not used
          //   context: { locale: 'en' }, // This is optional and defaults to an empty object if not used
          // })

          _.each(result.data.allWordpressWpProjects.edges, edge => {
            graphql(
              `
                {
                  allWordpressWpProjects(filter: { _function: { eq: 86 } }) {
                    edges {
                      node {
                        title
                        id
                        slug
                        _function
                        _place
                        _status
                        _year
                        featured_media {
                          localFile {
                            childImageSharp {
                              sizes(maxWidth: 680) {
                                base64
                                aspectRatio
                                src
                                srcSet
                                sizes
                              }
                            }
                          }
                        }
                        acf {
                          description
                        }
                      }
                    }
                  }
                }
              `
            ).then(similar => {
              if (similar.errors) {
                console.log(similar.errors)
                reject(similar.errors)
              }

              createPage({
                path: `/project/${edge.node.slug}/`,
                component: slash(projectTemplate),
                context: {
                  id: edge.node.id,

                  similar: similarPosts(
                    similar.data.allWordpressWpProjects.edges,
                    6
                  ),
                },
              })
            })

            // if (edge.node.wpml_translations.length !== 0) {
            //   edge.node.wpml_translations.forEach(lang => {
            //     createPage({
            //       path: `${lang.locale}/project/${edge.node.slug}/`,
            //       component: slash(projectTemplate),
            //       context: {
            //         id: edge.node.id,
            //         locale: lang.locale,
            //       },
            //     })
            //   })
            // }
          })
          resolve()
        })
      })
      // ==== END PROJECTS ====
      // ==== FUNCTIONS TAXONOMY (WORDPRESS NATIVE AND ACF) ====
      .then(() => {
        graphql(
          `
            {
              allWordpressWpFunction {
                edges {
                  node {
                    wordpress_id
                    slug
                    name
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }

          const projectsTemplate = path.resolve(
            './src/templates/taxonomies/func_taxonomy.js'
          )

          _.each(result.data.allWordpressWpFunction.edges, edge => {
            createPage({
              path: `/projects/func/${edge.node.slug}/`,
              component: slash(projectsTemplate),
              context: {
                wordpress_id: edge.node.wordpress_id,
                slug: edge.node.slug,
                name: edge.node.name,
              },
            })
          })
          resolve()
        })
      })
      // ==== END FUNCTIONS ====
      // ==== YEAR TAXONOMY (WORDPRESS NATIVE AND ACF) ====
      .then(() => {
        graphql(
          `
            {
              allWordpressWpYear {
                edges {
                  node {
                    wordpress_id
                    slug
                    name
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }

          const projectsTemplate = path.resolve(
            './src/templates/taxonomies/year_taxonomy.js'
          )

          _.each(result.data.allWordpressWpYear.edges, edge => {
            createPage({
              path: `/projects/year/${edge.node.slug}/`,
              component: slash(projectsTemplate),
              context: {
                wordpress_id: edge.node.wordpress_id,
                slug: edge.node.slug,
                name: edge.node.name,
              },
            })
          })
          resolve()
        })
      })
      // ==== END YEARS ====
      // ==== YEAR Status (WORDPRESS NATIVE AND ACF) ====
      .then(() => {
        graphql(
          `
            {
              allWordpressWpStatus {
                edges {
                  node {
                    wordpress_id
                    slug
                    name
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }

          const projectsTemplate = path.resolve(
            './src/templates/taxonomies/status_taxonomy.js'
          )

          _.each(result.data.allWordpressWpStatus.edges, edge => {
            createPage({
              path: `/projects/status/${edge.node.slug}/`,
              component: slash(projectsTemplate),
              context: {
                wordpress_id: edge.node.wordpress_id,
                slug: edge.node.slug,
                name: edge.node.name,
              },
            })
          })
          resolve()
        })
      })
      // ==== END Status ====
      // ==== YEAR Place (WORDPRESS NATIVE AND ACF) ====
      .then(() => {
        graphql(
          `
            {
              allWordpressWpPlace {
                edges {
                  node {
                    wordpress_id
                    slug
                    name
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }

          const projectsTemplate = path.resolve(
            './src/templates/taxonomies/place_taxonomy.js'
          )

          _.each(result.data.allWordpressWpPlace.edges, edge => {
            createPage({
              path: `/projects/place/${edge.node.slug}/`,
              component: slash(projectsTemplate),
              context: {
                wordpress_id: edge.node.wordpress_id,
                slug: edge.node.slug,
                name: edge.node.name,
              },
            })
          })
          resolve()
        })
      })
      // ==== END Place ====

      .then(() => {
        graphql(
          `
            {
              allWordpressWpSection {
                edges {
                  node {
                    wordpress_id
                    slug
                    name
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }

          const projectsTemplate = path.resolve(
            './src/templates/taxonomies/section_taxonomy.js'
          )

          _.each(result.data.allWordpressWpSection.edges, edge => {
            createPage({
              path: `/${edge.node.slug}/`,
              component: slash(projectsTemplate),
              context: {
                wordpress_id: edge.node.wordpress_id,
                slug: edge.node.slug,
                name: edge.node.name,
              },
            })
          })
          resolve()
        })
      })
      .then(() => {
        graphql(
          `
            {
              allWordpressWpObjects {
                edges {
                  node {
                    id
                    title
                    wordpress_id
                    slug
                    featured_media {
                      localFile {
                        childImageSharp {
                          sizes(maxWidth: 680) {
                            base64
                            aspectRatio
                            src
                            srcSet
                            sizes
                          }
                        }
                      }
                    }
                    acf {
                      description
                    }
                  }
                }
              }
            }
          `
        ).then(result => {
          if (result.errors) {
            console.log(result.errors)
            reject(result.errors)
          }

          const objectsTemplate = path.resolve('./src/templates/objects.js')
          const objectTemplate = path.resolve('./src/templates/object.js')

          createPage({
            path: `/design`,
            component: slash(objectsTemplate),
            context: {},
          })

          _.each(result.data.allWordpressWpObjects.edges, edge => {
            createPage({
              path: `/object/${edge.node.slug}/`,
              component: slash(objectTemplate),
              context: {
                id: edge.node.id,
                similar: similarPosts(
                  result.data.allWordpressWpObjects.edges,
                  6
                ),
              },
            })
          })
          resolve()
        })
      })
    // ==== END Place ====
  })
}
