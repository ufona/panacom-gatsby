import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Scrollbar from 'react-smooth-scrollbar'
import { initializeValues } from '../actions/filter'
import { initializeObjectValues } from '../actions/filterObject'
import './index.scss'

class Layout extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { dispatch } = this.props

    const {
      allWordpressWpStatus,
      allWordpressWpFunction,
      allWordpressWpPlace,
      allWordpressWpYear,
      allWordpressWpDesign,
    } = this.props.data

    const obj = {
      statuses: allWordpressWpStatus.edges,
      functions: allWordpressWpFunction.edges,
      places: allWordpressWpPlace.edges,
      years: allWordpressWpYear.edges,
    }

    dispatch(initializeObjectValues(allWordpressWpDesign.edges))
    dispatch(initializeValues(obj))
  }

  render() {
    const { data, children } = this.props
    return (
      <Fragment>
        <Helmet
          title="Panacom"
          meta={[
            { name: 'description', content: 'архитектура' },
            { name: 'keywords', content: 'архитектура' },
          ]}
        />
        <Header menu={data} />
        <div style={{ paddingTop: '65px' }}>{children()}</div>
        <Footer />
      </Fragment>
    )
  }
}
const mapStateToProps = state => {
  return { filter: state.filter }
}

const mapDispatchToProps = dispatch => ({
  initializeValues: obj => {
    dispatch(initializeValues(obj))
  },
})

export default connect(mapStateToProps)(Layout)

export const query = graphql`
  query LayoutQuery {
    allWordpressWpApiMenusMenusItems {
      edges {
        node {
          id
          name
          items {
            title
            url
            object_slug
          }
        }
      }
    }
    allWordpressWpStatus {
      edges {
        node {
          slug
          wordpress_id
          name
        }
      }
    }
    allWordpressWpFunction {
      edges {
        node {
          slug
          wordpress_id
          name
        }
      }
    }
    allWordpressWpPlace {
      edges {
        node {
          slug
          wordpress_id
          name
        }
      }
    }

    allWordpressWpYear {
      edges {
        node {
          slug
          wordpress_id
          name
        }
      }
    }
    allWordpressWpDesign {
      edges {
        node {
          slug
          wordpress_id
          name
        }
      }
    }
  }
`
