import React, { Fragment, Component } from 'react'
import Link from 'gatsby-link'
import Masonry from 'react-masonry-component'
import { connect } from 'react-redux'
import MainSlider from '../components/MainSlider'
import IndexProject from '../components/ProjectCard'
import ArticleCard from '../components/ArticleCard'
import Filter from '../components/Filter'
import { MoreButton } from '../components/MoreButton'
import filterProjects from '../utils/filterProjects'
import { paginationIncrement } from '../actions/pagination'

class Index extends React.Component {
  constructor() {
    super()
  }

  render() {
    const publications = this.props.data.allWordpressWpPublications.edges
    let counter = 0
    const projects = this.props.data.allWordpressWpProjects.edges
    const projectsLength = projects.length
    const projectsPrint = projects
      .filter(({ node }) => filterProjects(node, this.props.filter))
      .slice(0, this.props.page)
      .map(({ node }, index) => {
        if ((index + 1) % 5 == 0) {
          let publication = publications[counter].node
          counter += 1
          return (
            <Fragment>
              <ArticleCard
                name={publication.title}
                caption={publication.content}
                link={publication.acf.link}
                type={publication.acf.type}
                date={publication.date}
                image={
                  publication.featured_media.localFile.childImageSharp.sizes
                }
              />
              <IndexProject
                key={node.id}
                slug={node.slug}
                title={node.title}
                img={node.featured_media.localFile.childImageSharp.sizes}
                acf={node.acf}
                func={node._function}
              />
            </Fragment>
          )
        } else {
          return (
            <IndexProject
              key={node.id}
              slug={node.slug}
              title={node.title}
              img={node.featured_media.localFile.childImageSharp.sizes}
              acf={node.acf}
              func={node._function}
            />
          )
        }
      })

    return (
      <div id="home">
        <MainSlider />
        <Filter />
        <section className={'container projects'}>
          <Masonry onLayoutComplete={() => console.log('omg')} gutter={20}>
            {projectsPrint.map(e => e)}
          </Masonry>
          {projectsLength - this.props.page >= 0 &&
            this.props.filter.selected.length === 0 && (
              <MoreButton onClick={() => this.props.nextPage()} />
            )}
        </section>
      </div>
    )
  }
}

export const query = graphql`
  query indexProjectsQuery {
    allWordpressWpProjects {
      edges {
        node {
          title
          id
          slug
          _function
          _place
          _status
          _year
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 680) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
          acf {
            description
          }
        }
      }
    }
    allWordpressWpPublications {
      edges {
        node {
          title
          id
          content
          date
          acf {
            type
            link
          }
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 230) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
        }
      }
    }
  }
`
const mapDispatchToProps = dispatch => ({
  nextPage: () => {
    dispatch(paginationIncrement())
  },
})

const mapStateToProps = state => {
  return {
    filter: state.filter,
    page: state.pagination.postsToShow,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)
