import React from 'react'
import Link from 'gatsby-link'
import BuroNavHero from '../components/BuroNavHero'
import MemberCard from '../components/MemberCard'
import Masonry from 'react-masonry-component'
const MenuItems = [
  { title: 'бюро', url: '/buro', active: false, id: 1 },
  { title: 'команда', url: '/team', active: true, id: 2 },
  { title: 'награды', url: '/rewards', active: false, id: 3 },
  { title: 'публикации', url: '/publications', active: false, id: 4 },
]

const BuroPage = ({ data }) => (
  <div>
    <BuroNavHero
      title="команда"
      img={data.file.childImageSharp.sizes}
      menu={MenuItems}
    />
    <div className="container">
      <Masonry gutter={20}>
        {data.allWordpressWpMembers.edges.map(({ node }) => (
          <MemberCard
            name={node.title}
            caption={node.content}
            img={node.featured_media.localFile.childImageSharp.sizes}
            date={node.acf.year}
          />
        ))}
      </Masonry>
    </div>
  </div>
)
export const query = graphql`
  query indexMemberQuery {
    file(relativePath: { eq: "buro.png" }) {
      childImageSharp {
        sizes(maxWidth: 1240) {
          ...GatsbyImageSharpSizes_tracedSVG
        }
      }
    }
    allWordpressWpMembers {
      edges {
        node {
          title
          id
          content
          acf {
            year
          }
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 230) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
        }
      }
    }
  }
`

export default BuroPage
