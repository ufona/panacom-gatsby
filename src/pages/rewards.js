import React from 'react'
import Link from 'gatsby-link'
import BuroNavHero from '../components/BuroNavHero'
import PublicationCard from '../components/PublicationCard'
import Masonry from 'react-masonry-component'
import './buro.scss'
import buro from './img/buro.png'
const MenuItems = [
  { title: 'бюро', url: '/buro', active: false, id: 1 },
  { title: 'команда', url: '/team', active: false, id: 2 },
  { title: 'награды', url: '/rewards', active: true, id: 3 },
  { title: 'публикации', url: '/publications', active: false, id: 4 },
]

const AwardsPage = ({ data }) => (
  <div>
    <BuroNavHero
      title="награды"
      img={data.file.childImageSharp.sizes}
      menu={MenuItems}
    />
    <div className="container rewards-page">
      <Masonry gutter={20}>
        {data.allWordpressWpRewards.edges.map(({ node }) => (
          <PublicationCard
            name={node.title}
            caption={node.content}
            img={node.featured_media.localFile.childImageSharp.sizes}
          />
        ))}
      </Masonry>
    </div>
  </div>
)

export const query = graphql`
  query indexAwardsQuery {
    file(relativePath: { eq: "buro.png" }) {
      childImageSharp {
        sizes(maxWidth: 1240) {
          ...GatsbyImageSharpSizes_tracedSVG
        }
      }
    }
    allWordpressWpRewards(limit: 56) {
      edges {
        node {
          title
          id
          content
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 230) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
        }
      }
    }
  }
`

export default AwardsPage
