import React, { Fragment } from 'react'
import Link from 'gatsby-link'
import BuroNavHero from '../components/BuroNavHero'
import './buro.scss'
import buro from './img/buro.png'
const MenuItems = [
  { title: 'бюро', url: '/buro', active: true, id: 1 },
  { title: 'команда', url: '/team', active: false, id: 2 },
  { title: 'награды', url: '/rewards', active: false, id: 3 },
  { title: 'публикации', url: '/publications', active: false, id: 4 },
]

const BuroPage = ({ data }) => (
  <Fragment>
    <BuroNavHero
      title="бюро"
      img={data.file.childImageSharp.sizes}
      menu={MenuItems}
    />
    <div className="container buro-page">
      <h1>Архитектурное бюро PANACOM</h1>
      <p>
        Бюро создано в 2000 году Никитой Токаревым и Арсением Леоновичем.
        Основные направления работы: городское планирование, проектирова- ние
        жилых и общественных зданий, загородных домов и интерьеров, выставочные
        проекты и промышленный дизайн. Выполняются все смежные разделы проекта:
        конструкции, инженерные системы, ландшафтный дизайн, авторский надзор за
        строительством.
      </p>
      <p>
        Сотрудники PANACOM, кроме образования в России, проходили стажи- ровки
        за рубежом, в т.ч. в Дельфтском технологическом институте, Институте
        жилища и городского развития в Роттердаме, в Институте Искусств Saint
        Martins в Лондоне.
      </p>
      <p>
        Среди проектов и построек частные и общественные здания в Москве,
        Подмосковье, других регионах России, в Англии и Германии. Предмет- ный
        дизайн выполнялся для компаний: Valli & Valli (Италия), Grohe (Гер-
        мания), Fratelli Boffi (Италия), Kantarutti (Италия), Atelier Sedap
        (Франция), Nayada (Россия), Световые технологии (Россия) и других.
      </p>
      <p>
        Бюро PANACOM - лауреат и победитель многих российских и междуна- родных
        архитектурных конкурсов, неоднократный номинант и лауреат премии
        «Золотое сечение», обладатель премий в области градострои- тельства,
        архитектуры, интерьерного, выставочного и предметного дизайна.
      </p>
    </div>
  </Fragment>
)

export const query = graphql`
  query indexBuroQuery {
    file(relativePath: { eq: "buro.png" }) {
      childImageSharp {
        sizes(maxWidth: 1240) {
          ...GatsbyImageSharpSizes_tracedSVG
        }
      }
    }
  }
`

export default BuroPage
