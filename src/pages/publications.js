import React from 'react'
import Link from 'gatsby-link'
import BuroNavHero from '../components/BuroNavHero'
import PublicationCard from '../components/PublicationCard'
import Masonry from 'react-masonry-component'
import publications from './img/publications.png'
const MenuItems = [
  { title: 'бюро', url: '/buro', active: false, id: 1 },
  { title: 'команда', url: '/team', active: false, id: 2 },
  { title: 'награды', url: '/rewards', active: false, id: 3 },
  { title: 'публикации', url: '/publications', active: true, id: 4 },
]

const BuroPage = ({ data }) => (
  <div>
    <BuroNavHero
      title="публикации"
      img={data.file.childImageSharp.sizes}
      menu={MenuItems}
    />
    <div className="container">
      <Masonry gutter={20}>
        {data.allWordpressWpPublications.edges.map(({ node }) => (
          <PublicationCard
            name={node.title}
            caption={node.content}
            img={node.featured_media.localFile.childImageSharp.sizes}
            link={node.acf.link}
          />
        ))}
      </Masonry>
    </div>
  </div>
)

export const query = graphql`
  query indexPublicationsQuery {
    file(relativePath: { eq: "publications.png" }) {
      childImageSharp {
        sizes(maxWidth: 1240) {
          ...GatsbyImageSharpSizes_tracedSVG
        }
      }
    }
    allWordpressWpPublications {
      edges {
        node {
          title
          id
          content
          acf {
            link
          }
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 230) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
        }
      }
    }
  }
`

export default BuroPage
