import {
  PAGINATION_RESET,
  PAGINATION_INCREMENT,
} from '../constants/ActionTypes'

const paginationReset = () => ({
  type: PAGINATION_RESET,
})

const paginationIncrement = () => ({
  type: PAGINATION_INCREMENT,
})

export { paginationIncrement, paginationReset }
