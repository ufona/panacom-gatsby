import {
  OBJECT_FILTER_CHANGE,
  OBJECT_FILTER_INIT,
  OBJECT_FILTER_RESET,
} from '../constants/ActionTypes'

const change = payload => {
  if (payload == 97) return resetValues
  return {
    type: OBJECT_FILTER_CHANGE,
    payload: payload,
  }
}

const resetFilter = () => ({
  type: OBJECT_FILTER_RESET,
})

const resetValues = {
  type: OBJECT_FILTER_RESET,
}

const initializeObjectValues = payload => ({
  type: OBJECT_FILTER_INIT,
  payload: payload,
})

export default {
  change,
  resetFilter,
  initializeObjectValues,
}
