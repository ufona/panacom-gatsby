import {
  CHANGE_PLACE,
  CHANGE_STATUS,
  CHANGE_YEAR,
  CHANGE_FUNCTION,
  FILTER_INIT,
  FILTER_RESET,
} from '../constants/ActionTypes'

const changePlace = place => {
  if (place.value == 90) return resetValues
  return {
    type: CHANGE_PLACE,
    payload: place,
  }
}

const changeStatus = status => {
  if (status.value == 85) return resetValues
  return {
    type: CHANGE_STATUS,
    payload: status,
  }
}

const changeYear = year => {
  if (year.value == 89) return resetValues
  return {
    type: CHANGE_YEAR,
    payload: year,
  }
}

const changeFunction = func => {
  if (func.value == 86) return resetValues
  return {
    type: CHANGE_FUNCTION,
    payload: func,
  }
}
const resetFilter = () => ({
  type: FILTER_RESET,
})

const resetValues = {
  type: FILTER_RESET,
}

const initializeValues = obj => ({
  type: FILTER_INIT,
  payload: obj,
})

export default {
  changePlace,
  changeStatus,
  changeYear,
  changeFunction,
  initializeValues,
  resetFilter,
}
