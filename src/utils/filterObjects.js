const filterObjects = (node, filterObject) =>
  node.design.includes(+filterObject.selected)
export default filterObjects
