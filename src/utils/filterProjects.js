const filterProjects = (node, filter) =>
  node._function.includes(+filter.function.value) &&
  node._place.includes(+filter.place.value) &&
  node._year.includes(+filter.year.value) &&
  node._status.includes(+filter.status.value)
export default filterProjects
