import React, { Component } from 'react'
import Link from 'gatsby-link'
import './index.scss'
const MainMenu = ({ data, hideMenu }) => (
  <div className="menu">
    <ul>
      {data.map(item => (
        <li key={item.object_slug}>
          <Link onClick={hideMenu} to={item.url}>
            {item.title}
          </Link>
        </li>
      ))}
    </ul>
  </div>
)

export default MainMenu
