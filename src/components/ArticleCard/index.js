import React from 'react'
import Link from 'gatsby-link'
import './index.scss'
import img from './a.png'
import Img from 'gatsby-image'

const options = {
  year: 'numeric',
  month: 'long',
  day: 'numeric',
}

const ArticleCard = ({ name, caption, date, image, color, type, link }) => {
  let time = new Date(date)
  return (
    <div className={'article-card grid__item' + ' ' + color}>
      <a target="_blank" href={link} className="article-card__inner">
        <time className="article-card__meta">
          {time.toLocaleString('ru', options)}
        </time>

        <h3
          className="article-card__title"
          dangerouslySetInnerHTML={{ __html: name }}
        />

        <div className="article-card__image">
          <Img
            key={image.src}
            sizes={image}
            style={{
              position: 'absolute',
              left: 0,
              top: 0,
              width: '100%',
              height: '100%',
            }}
          />
        </div>

        <div
          className="article-card__caption"
          dangerouslySetInnerHTML={{ __html: caption }}
        />
        <p className="article-card__category">{type ? type : 'публикации'}</p>
      </a>
    </div>
  )
}

export default ArticleCard
