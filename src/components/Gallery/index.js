import React, { Fragment } from 'react'
import './index.scss'

const Lightbox = props => (
  <Fragment>
    {props.images.map((obj, index) => [
      <div key="image">
        <a className="imglink" href={'#LB_target_' + index}>
          <img id={'LB_' + index} src={obj.img} alt={obj.caption} />
        </a>
      </div>,
      <a
        className="WrapperLink"
        key="link"
        href={'#LB_' + index}
        id={'LB_target_' + index}
      >
        <div className="ImageContainer">
          <img src={obj.img} />
        </div>
      </a>,
    ])}
  </Fragment>
)
export default Lightbox
