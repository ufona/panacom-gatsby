import React from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'
import './index.scss'

export default ({ open }) => (
  <div className={!open ? 'menu-icon' : 'menu-icon clicked'}>
    <div className="dot" />
    <div className="dot" />
    <div className="dot" />
    <div className="dot" />
    <div className="dot" />
    <div className="dot" />
    <div className="dot" />
    <div className="dot" />
    <div className="dot" />
  </div>
)
