import React from 'react'
import Link from 'gatsby-link'
import './style.scss'
import image from './pub.png'
import Fade from 'react-reveal/Fade'
import Img from 'gatsby-image'

const PublicationCard = ({ name, caption, date, img, link }) => (
  <div className="publication-card grid__item">
    <Fade>
      <div className="publication-card__inner">
        <Img key={img.src} sizes={img} />
        <a
          target="_blank"
          href={link}
          className="publication-card__description"
        >
          <h3
            className="publication-card__name"
            dangerouslySetInnerHTML={{ __html: name }}
          />
          <p
            className="publication-card__caption"
            dangerouslySetInnerHTML={{ __html: caption }}
          />
        </a>
      </div>
    </Fade>
  </div>
)

export default PublicationCard
