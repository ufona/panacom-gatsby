import React from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'
import './index.scss'
import Fade from 'react-reveal/Fade'
export default ({
  title,
  slug,
  img,
  acf,
  type = 'project',
  similar = false,
}) => (
  <Link
    to={'/' + type + '/' + slug}
    className={
      similar
        ? 'project-card grid__item project-card--similar'
        : 'project-card grid__item'
    }
  >
    <div className="project-card__image">
      <Fade>
        <Img key={img.src} sizes={img} />
      </Fade>
      <Fade bottom>
        <div data-title={title} className="project-card__title">
          <span
            data-title={title}
            dangerouslySetInnerHTML={{
              __html: title,
            }}
          />
        </div>
      </Fade>
    </div>
    <Fade bottom>
      <div className="project-card__caption">{acf.description}</div>
    </Fade>
  </Link>
)
