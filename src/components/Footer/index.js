import React from 'react'
import Link from 'gatsby-link'
import './index.scss'
const Footer = () => (
  <footer className="footer">
    <div className="footer__info">
      <div className="footer__item tel">
        <h4>звоните</h4>
        <a href="tel:+7 901 180-5484">+7 901 180-5484</a>
      </div>
      <div className="footer__line" />
      <div className="footer__item email">
        <h4>пишите</h4>
        <a href="mailto:info@pana.com.ru">info@pana.com.ru</a>
      </div>
      <div className="footer__line" />
      <div className="footer__item social">
        <h4>подписывайтесь</h4>
        <a href="https://www.facebook.com/pana.com.ru/">*facebook</a>
      </div>
    </div>
    <div className="footer__authors">
      <p>
        design - Yra Nemov<br />
        developed - Anton Postoyalko
      </p>
    </div>
  </footer>
)

export default Footer
