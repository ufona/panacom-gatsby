import React from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'
import './index.scss'

const toTop = () => {
  window.scrollTo(0, 0)
}

const MoreButtonLink = ({ url }) => (
  <Link to={url} className="more-button">
    <span>ЕЩЁ</span>
  </Link>
)

const MoreButton = ({ onClick }) => (
  <button onClick={onClick} className="more-button">
    <span>ЕЩЁ</span>
  </button>
)

export { MoreButtonLink, MoreButton }
