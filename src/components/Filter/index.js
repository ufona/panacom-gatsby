import React, { Component } from 'react'
import Link from 'gatsby-link'
import Select, { components } from 'react-select'
import './index.scss'
import {
  changePlace,
  changeStatus,
  changeYear,
  changeFunction,
  resetFilter,
} from '../../actions/filter'
import { connect } from 'react-redux'

const DropdownIndicator = props => {
  return (
    <components.DropdownIndicator {...props}>
      <span style={{ padding: '0 5px' }}> + </span>
    </components.DropdownIndicator>
  )
}

const customStyles = {
  option: (base, state) => ({
    ...base,
    borderBottom: '1px solid hsl(0,0%,80%)',
    padding: '10px',
    fontSize: '17px',
    backgroundColor: state.isSelected
      ? '#80a1c7'
      : state.isFocused
        ? '#80a1c766'
        : 'white',
  }),
  control: (base, state) => ({
    ...base,
    border: 'none',
    background: 'none',
    fontSize: '17px',
  }),
}

const customStylesDisabled = {
  option: (base, state) => ({
    ...base,
    borderBottom: '1px solid hsl(0,0%,80%)',
    padding: '10px',
    fontSize: '17px',
    backgroundColor: state.isSelected
      ? '#80a1c7'
      : state.isFocused
        ? '#80a1c766'
        : 'white',
  }),
  control: (base, state) => ({
    ...base,
    border: 'none',
    background: 'none',
    fontSize: '17px',
  }),
  singleValue: () => ({ color: 'grey' }),
}

const disableOther = (filter, type) => {
  if (filter !== type) return filter == '' ? customStyles : customStylesDisabled
  else return customStyles
}

const Filter = ({
  filter,
  changePlace,
  changeStatus,
  changeYear,
  changeFunction,
  resetFilter,
  disable,
}) => (
  <div className="filter">
    <Select
      name="place"
      styles={disableOther(filter.selected, 'place')}
      components={{ DropdownIndicator }}
      value={filter.place}
      onMenuOpen={resetFilter}
      onChange={changePlace}
      className="filter-select"
      classNamePrefix="filter-select"
      options={filter.values.places.map(item => ({
        value: item.node.wordpress_id,
        label: item.node.name,
      }))}
    />
    <Select
      name="function"
      components={{ DropdownIndicator }}
      value={filter.function}
      onMenuOpen={resetFilter}
      onChange={changeFunction}
      styles={disableOther(filter.selected, 'function')}
      className="filter-select"
      classNamePrefix="filter-select"
      options={filter.values.functions.map(item => ({
        value: item.node.wordpress_id,
        label: item.node.name,
      }))}
    />
    <Select
      name="status"
      components={{ DropdownIndicator }}
      value={filter.status}
      onMenuOpen={resetFilter}
      onChange={changeStatus}
      styles={disableOther(filter.selected, 'status')}
      className="filter-select"
      classNamePrefix="filter-select"
      options={filter.values.statuses.map(item => ({
        value: item.node.wordpress_id,
        label: item.node.name,
      }))}
    />
  </div>
)

// <Select
// name="year"
// components={{ DropdownIndicator }}
// value={filter.year}
// onChange={changeYear}
// isDisabled={disable === 'year' ? true : false}
// className="filter-select"
// classNamePrefix="filter-select"
// options={filter.values.years.map(item => ({
//   value: item.node.wordpress_id,
//   label: item.node.name,
// }))}
// />

const mapDispatchToProps = dispatch => ({
  changePlace: value => {
    dispatch(changePlace(value))
  },
  changeStatus: value => {
    dispatch(changeStatus(value))
  },
  changeYear: value => {
    dispatch(changeYear(value))
  },
  changeFunction: value => {
    dispatch(changeFunction(value))
  },
  resetFilter: () => {
    dispatch(resetFilter())
  },
})

const mapStateToProps = state => {
  return {
    filter: state.filter,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filter)
