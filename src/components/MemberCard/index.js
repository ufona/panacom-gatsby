import React from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'
import './style.scss'
import image from './men.png'
import Fade from 'react-reveal/Fade'
const MemberCard = ({ name, caption, date, img }) => (
  <div className="member-card grid__item">
    <Fade>
      <div className="member-card__inner">
        <Img key={img.src} fadeIn={true} sizes={img} />

        <h3 className="member-card__name">{name}</h3>
        <p
          className="member-card__caption"
          dangerouslySetInnerHTML={{ __html: caption }}
        />
        <p className="member-card__date">{date ? date : ''}</p>
      </div>
    </Fade>
  </div>
)

export default MemberCard
