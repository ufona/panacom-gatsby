import React from 'react'
import './index.scss'
import {
  change,
  changeStatus,
  changeYear,
  changeFunction,
} from '../../actions/filterObject'
import { connect } from 'react-redux'

const Filter = ({ values, change, selected }) => (
  <ul className="filter">
    {values.map(({ node }) => (
      <li
        className={node.wordpress_id === selected ? 'selected' : ''}
        onClick={() => change(node.wordpress_id)}
      >
        {node.name}
      </li>
    ))}
  </ul>
)

const mapDispatchToProps = dispatch => ({
  change: value => {
    dispatch(change(value))
  },
})

const mapStateToProps = state => {
  return {
    values: state.filterObject.values,
    selected: state.filterObject.selected,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter)
