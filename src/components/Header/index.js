import React, { Component, Fragment } from 'react'
import Link from 'gatsby-link'
import './index.scss'
import logo from './logo-t.png'
import MainMenu from '../Menu'
import MenuButton from '../MenuButton'
import throttle from '../../utils/throttle'
class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      header: true,
    }

    this.navbarHeight = 65
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll())
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll())
  }

  handleScroll = event => throttle(this.hasScrolled, 250)

  hideMenu = () => this.setState({ open: false })

  openMenu = () => this.setState({ open: !this.state.open })

  hideHeader = () =>
    this.setState({
      header: false,
    })

  showHeader = () =>
    this.setState({
      header: true,
    })

  getDocHeight() {
    return Math.max(
      document.body.scrollHeight,
      document.documentElement.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.offsetHeight,
      document.body.clientHeight,
      document.documentElement.clientHeight
    )
  }

  hasScrolled = () => {
    const st = window.scrollY
    // Make sure they scroll more than delta
    if (Math.abs(this.state.lastScrollTop - st) <= 5) return

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > this.state.lastScrollTop && st > this.navbarHeight && st > 300) {
      // Scroll Down
      this.hideHeader()
    } else if (st < 300 || st < this.getDocHeight()) {
      this.showHeader()
    }

    this.setState({
      lastScrollTop: st,
    })
  }

  render() {
    const data = this.props.menu.allWordpressWpApiMenusMenusItems.edges[1].node
      .items
    // console.log(this.state.header, this.state.lastScrollTop)
    return (
      <Fragment>
        <header
          id="header"
          className={
            this.state.header || this.state.open ? 'header' : 'header closed'
          }
        >
          <div className="header__inner">
            <div className="header__logo">
              <Link onClick={this.hideMenu} to={'/'}>
                <img src={logo} alt="logo" />
              </Link>
            </div>

            <div className="header__menu">
              <button onClick={this.openMenu}>
                <MenuButton open={this.state.open} />
                <span>МЕНЮ</span>
              </button>
            </div>

            <div className="header__utils">
              <button className="header__search">
                <span className="header__search_icon" />
                поиск
              </button>
              <div className="header__translate">Ru &lt; &gt; En</div>
            </div>
          </div>
        </header>
        <nav
          className={
            !this.state.open ? 'header__nav' : 'header__nav' + ' ' + 'open'
          }
        >
          <div className="header__nav__inner">
            <MainMenu hideMenu={this.hideMenu} data={data} />
            <Link
              className="header__nav__buro"
              onClick={this.hideMenu}
              to={'/buro'}
            >
              /БЮРО
            </Link>
          </div>
        </nav>
      </Fragment>
    )
  }
}

export default Header
