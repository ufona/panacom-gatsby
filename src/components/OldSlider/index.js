import React, { Component } from 'react'
import './index.css'

export default class MainSlider extends Component {
  constructor() {
    super()

    this.state = {
      hovered: false,

      items: [
        {
          title: 'София',
          img: 'http://panacom.surge.sh/img/main-2.png',
          caption:
            'Жилой комплекс на территории бывшего <br />вагоноремонтного завода',
          slug: 'asd',
        },
        {
          title: 'Путь к реке',
          img: 'http://panacom.surge.sh/img/main-1.png',
          caption:
            'Жилой комплекс на территории бывшего <br />вагоноремонтного завода',
          slug: 'asd',
        },
        {
          title: 'Софияzzz',
          img: 'http://panacom.surge.sh/img/main-2.png',
          caption:
            'Жилой комплекс на территории бывшего <br />вагоноремонтного завода',
          slug: 'asd',
        },
        {
          title: 'Путь к рекеzzz',
          img: 'http://panacom.surge.sh/img/main-1.png',
          caption:
            'Жилой комплекс на территории бывшего <br />вагоноремонтного завода',
          slug: 'asd',
        },
      ],
    }
  }

  componentDidMount() {
    this.state.items.forEach((x, i) => {
      this.state.items[i].type = 'far'
    })
    this.state.items[0].type = 'active'
    this.state.items[1].type = 'next'
    this.setState({ items: this.state.items })
  }

  goSlide = () => {
    let items = this.state.items
    let length = items.length
    let active = items.findIndex(x => x.type === 'active')
    let next = active + 2 > length ? 0 : active + 1
    let far = active + 2 === length ? 0 : active + 2 > length ? 1 : active + 2

    items[active].type = 'far'

    items[next].type = 'active'

    items[far].type = 'next'

    this.setState({ items: items })
  }

  toogleHover = () => this.setState({ hovered: !this.state.hovered })

  render() {
    return (
      <section className={this.state.hovered ? 'promo hover' : 'promo'}>
        <div className="promo__content">
          {this.state.items.map((item, index) => (
            <a
              className={
                'promo__item promo__item_dark promo__item_' + item.type
              }
              key={index}
              href="#"
            >
              <span className="promo__item__bg">
                <i
                  className="promo__item__bg__picture"
                  style={{ backgroundImage: `url(${item.img})` }}
                />
              </span>
              <span className="promo__item__about">
                <span className="promo__item__about__content">
                  <h2 className="promo__item__about__title">{item.title}</h2>
                  <p className="promo__item__about__caption">{item.caption}</p>
                  <span className="promo__item__about__action">
                    смотреть проект
                  </span>
                </span>
              </span>
            </a>
          ))}
        </div>
        <div
          className="promo__next"
          onMouseEnter={this.toogleHover}
          onMouseLeave={this.toogleHover}
          onClick={this.goSlide}
        />
      </section>
    )
  }
}
