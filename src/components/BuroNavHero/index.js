import React from 'react'
import './index.scss'
import Link from 'gatsby-link'
import Fade from 'react-reveal/Fade'
import Img from 'gatsby-image'
const BuroNavHero = ({ title, menu, img }) => (
  <Fade>
    <div className="buro">
      <Img key={img.src} sizes={img} />
      <Fade top>
        <h1 className="buro__title">{title}</h1>
      </Fade>
      <Fade bottom>
        <div className="buro__nav">
          <nav>
            <ul>
              {menu.map(item => (
                <li className={item.active ? 'active' : ''} key={item.id}>
                  <Link to={item.url}>{item.title}</Link>
                </li>
              ))}
            </ul>
          </nav>
        </div>
      </Fade>
    </div>
  </Fade>
)

export default BuroNavHero
