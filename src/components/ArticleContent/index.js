import React, { Component } from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'
import './index.scss'
import './grid.css'
import Lightbox from '../Gallery'
import Slider from 'react-slick'
import Fade from 'react-reveal/Fade'
import { ParallaxProvider } from 'react-scroll-parallax'
import IndexProject from '../ProjectCard'
import { Parallax } from 'react-scroll-parallax'
import Masonry from 'react-masonry-component'
const settings = {
  dots: false,
  infinite: true,
  arrows: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
}

export default ({ title, content, acf, similar, object = false }) => (
  <ParallaxProvider>
    <div className="project-content">
      <div className="project-content__header">
        <p
          dangerouslySetInnerHTML={{
            __html: acf.stages ? acf.stages : 'Проект',
          }}
        />
        <h1
          className=""
          dangerouslySetInnerHTML={{
            __html: title + '  |  ' + acf.description,
          }}
        />
      </div>
      {acf.header_image !== null && (
        <Fade>
          <div className="hero-container">
            <Parallax offsetYMin={'-20%'} offsetYMax={'20%'} slowerScrollRate>
              <img src={acf.header_image.source_url} alt="header-image" />
            </Parallax>
          </div>
        </Fade>
      )}
      {/* {acf.slider !== null && (
      <Slider {...settings}>
        {acf.slider.map(item => <img src={item.source_url} />)}
      </Slider>
    )} */}
      <div
        className="project-content__inner fw-container"
        dangerouslySetInnerHTML={{ __html: content }}
      />

      <section className="project-content__similar">
        <h1>Похожие проекты</h1>
        <div className="similar__inner">
          <Masonry gutter={20}>
            {similar.map(({ node }) => (
              <IndexProject
                key={node.id}
                slug={node.slug}
                title={node.title}
                img={node.featured_media.localFile.childImageSharp.sizes}
                acf={node.acf}
                type={object ? 'object' : 'project'}
              />
            ))}
          </Masonry>
        </div>
      </section>
    </div>
  </ParallaxProvider>
)
