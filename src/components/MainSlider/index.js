import React, { Component } from 'react'
import './index.scss'
import Link from 'gatsby-link'
import Fade from 'react-reveal/Fade'
import throttle from 'lodash/throttle'
import debounce from 'lodash/debounce'

export default class TestSlider extends Component {
  constructor() {
    super()

    this.method = throttle(this.goSlide, 700, {
      leading: true,
      trailing: false,
    })

    this.state = {
      hovered: false,
      items: [
        {
          title: 'Фермерия',
          img:
            'http://panacom.u3284.green.elastictech.org/wp-content/uploads/slaider/fermeria_01.png',
          caption: 'Гастрономический рынок на ВДНХ',
          slug: 'fermeriya',
        },
        {
          title: 'София',
          img:
            'http://panacom.u3284.green.elastictech.org/wp-content/uploads/slaider/sophia_04.png',
          caption:
            'Жилой комплекс на территории бывшего вагоноремонтного завода',
          slug: 'sofiya',
        },
        {
          title: 'Сны города',
          img:
            'http://panacom.u3284.green.elastictech.org/wp-content/uploads/slaider/sni_goroda_2.png',
          caption: 'Конкурсная концепция для деревни Палкино на границе Твери',
          slug: 'sny-goroda',
        },
        {
          title: 'Дом-Питон',
          img:
            'http://panacom.u3284.green.elastictech.org/wp-content/uploads/slaider/dom_piton_03.png',
          caption: 'Индивидуальный жилой дом',
          slug: 'dom-piton',
        },
        {
          title: 'OSKO CITY',
          img:
            'http://panacom.u3284.green.elastictech.org/wp-content/uploads/2018/07/3.jpg',
          caption: 'Офисный центр на Новой Риге',
          slug: 'osko-city',
        },
      ],
    }
  }

  componentWillMount() {
    this.state.items.forEach((x, i) => {
      this.state.items[i].type = 'far'
    })
    this.state.items[0].type = 'active'
    this.state.items[1].type = 'next'
    this.setState({ items: this.state.items })
  }

  goSlide = () => {
    let items = this.state.items
    let length = items.length
    let active = items.findIndex(x => x.type === 'active')
    let next = active + 1 === length ? 0 : active + 1
    let far = next + 1 === length ? 0 : next + 1
    let old = far + 1 === length ? 0 : far + 1
    items[active].type = 'old'
    items[next].type = 'active'
    items[far].type = 'next'
    items[old].type = 'far'
    this.setState({ items: items })
  }

  toogleHover = () => this.setState({ hovered: !this.state.hovered })

  render() {
    return (
      <React.Fragment>
        <div className="slidernext" onClick={this.method} />
        <div id="slider" className={this.state.hovered ? 'hover' : ''}>
          {this.state.items.map((item, index) => (
            <div className={'item ' + item.type}>
              <Fade>
                <div className={'content'}>
                  <Link to={'/project/' + item.slug} className="content__inner">
                    <h1>{item.title}</h1>
                    <p>{item.caption}</p>
                  </Link>
                </div>
              </Fade>
              <Fade>
                <div className="background">
                  <i
                    style={{
                      backgroundImage: `url(${item.img})`,
                    }}
                  />
                </div>
              </Fade>
            </div>
          ))}
        </div>
      </React.Fragment>
    )
  }
}
