import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ArticleContent from '../components/ArticleContent'

const ProjectTemplate = ({ data, pathContext }) => {
  // console.log(pathContext)
  // const { locale } = pathContext
  // console.log(locale)
  // if (locale == 'en') {
  //   return (
  //     <ArticleContent
  //       title={data.wordpressWpProjects.wpml_translations[0].post_title}
  //       content={data.wordpressWpProjects.wpml_translations[0].post_content}
  //       acf={acf}
  //     />
  //   )
  // } else {
  return (
    <ArticleContent
      title={data.wordpressWpProjects.title}
      content={data.wordpressWpProjects.content}
      acf={data.wordpressWpProjects.acf}
      similar={pathContext.similar}
    />
  )
  // }
}
//<img src={post.image.sizes.thumbnail} />

ProjectTemplate.propTypes = {
  data: PropTypes.object.isRequired,
  edges: PropTypes.array,
}

export default ProjectTemplate

export const pageQuery = graphql`
  query currentWpProjectQuery($id: String!) {
    wordpressWpProjects(id: { eq: $id }) {
      title
      content
      wpml_translations {
        post_title
        post_content
      }
      acf {
        typeyear
        stages
        description
        slider {
          source_url
        }
        header_image {
          source_url
        }
      }
    }
    site {
      siteMetadata {
        title
        subtitle
      }
    }
  }
`
