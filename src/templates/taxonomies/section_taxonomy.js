import React, { Component, Fragment } from 'react'
import Link from 'gatsby-link'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Masonry from 'react-masonry-component'
import IndexProject from '../../components/ProjectCard'
import ArticleCard from '../../components/ArticleCard'
import Filter from '../../components/Filter'
import { MoreButton } from '../../components/MoreButton'
import filterProjects from '../../utils/filterProjects'
import { paginationIncrement, paginationReset } from '../../actions/pagination'
import MainSlider from '../../components/MainSlider'
class SectionFilterTemplate extends Component {
  constructor(props) {
    super(props)
    this.props.resetPage()
  }

  render() {
    const publications = this.props.data.allWordpressWpPublications.edges
    let counter = 0

    const projects = this.props.data.allWordpressWpProjects.edges
    const projectsLength = projects.length
    const projectsPrint = projects
      .filter(({ node }) => filterProjects(node, this.props.filter))
      .slice(0, this.props.page)
      .map(({ node }, index) => {
        if ((index + 1) % 5 == 0) {
          let publication = publications[counter].node
          counter += 1
          return (
            <Fragment>
              <ArticleCard
                name={publication.title}
                caption={publication.content}
                link={publication.acf.link}
                type={publication.acf.type}
                date={publication.date}
                color={this.props.pathContext.wordpress_id == 82 ? 'green' : ''}
                image={
                  publication.featured_media.localFile.childImageSharp.sizes
                }
              />
              <IndexProject
                key={node.id}
                slug={node.slug}
                title={node.title}
                img={node.featured_media.localFile.childImageSharp.sizes}
                acf={node.acf}
                func={node._function}
              />
            </Fragment>
          )
        } else {
          return (
            <IndexProject
              key={node.id}
              slug={node.slug}
              title={node.title}
              img={node.featured_media.localFile.childImageSharp.sizes}
              acf={node.acf}
              func={node._function}
            />
          )
        }
      })

    return (
      <div>
        {this.props.pathContext.wordpress_id == 82 && <MainSlider />}
        <Filter />
        <section className={'container projects'}>
          <Masonry gutter={20}>{projectsPrint.map(e => e)}</Masonry>
          {projectsLength - this.props.page >= 0 &&
            this.props.filter.selected.length === 0 && (
              <MoreButton onClick={() => this.props.nextPage()} />
            )}
        </section>
      </div>
    )
  }
}

SectionFilterTemplate.propTypes = {
  data: PropTypes.object.isRequired,
  edges: PropTypes.array,
}

const mapDispatchToProps = dispatch => ({
  nextPage: () => {
    dispatch(paginationIncrement())
  },
  resetPage: () => {
    dispatch(paginationReset())
  },
})

const mapStateToProps = state => {
  return { filter: state.filter, page: state.pagination.postsToShow }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  SectionFilterTemplate
)
export const pageQuery = graphql`
  query sectionFilterQuery($wordpress_id: Int!) {
    allWordpressWpProjects(filter: { section: { eq: $wordpress_id } }) {
      edges {
        node {
          title
          id
          slug
          _function
          _place
          _status
          _year
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 680) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
          acf {
            description
          }
        }
      }
    }
    allWordpressWpPublications {
      edges {
        node {
          title
          id
          content
          date
          acf {
            type
            link
          }
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 230) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
        }
      }
    }
  }
`
