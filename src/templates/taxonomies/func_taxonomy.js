import React, { Component } from 'react'
import Link from 'gatsby-link'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Masonry from 'react-masonry-component'
import IndexProject from '../../components/ProjectCard'
import Filter from '../../components/Filter'
import filterProjects from '../../utils/filterProjects'

import { changeFunction, resetFilter } from '../../actions/filter'

class FuncFilterTemplate extends Component {
  constructor(props) {
    super(props)
    this.props.resetFilter()
    this.props.changeFunction({
      value: this.props.pathContext.wordpress_id,
      label: this.props.pathContext.name,
    })
  }
  render() {
    return (
      <div>
        <section className={'container'}>
          <Filter disable={'func'} />
          <Masonry gutter={20}>
            {this.props.data.allWordpressWpProjects !== null &&
              this.props.data.allWordpressWpProjects.edges
                .filter(({ node }) => filterProjects(node, this.props.filter))
                .map(({ node }) => (
                  <IndexProject
                    key={node.id}
                    slug={node.slug}
                    title={node.title}
                    img={node.featured_media.localFile.childImageSharp.sizes}
                    acf={node.acf}
                  />
                ))}
          </Masonry>
        </section>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  changeFunction: value => {
    dispatch(changeFunction(value))
  },
  resetFilter: () => {
    dispatch(resetFilter())
  },
})

const mapStateToProps = state => {
  return { filter: state.filter }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FuncFilterTemplate)

export const pageQuery = graphql`
  query funcFilterQuery($wordpress_id: Int!) {
    allWordpressWpProjects(filter: { _function: { eq: $wordpress_id } }) {
      edges {
        node {
          title
          id
          slug
          _function
          _place
          _status
          _year
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 680) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
          acf {
            description
          }
        }
      }
    }
  }
`
