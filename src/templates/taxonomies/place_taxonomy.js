import React, { Component } from 'react'
import Link from 'gatsby-link'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Masonry from 'react-masonry-component'
import IndexProject from '../../components/ProjectCard'
import Filter from '../../components/Filter'
import filterProjects from '../../utils/filterProjects'

import { changePlace } from '../../actions/filter'
class PlaceFilterTemplate extends Component {
  constructor(props) {
    super(props)
    this.props.changePlace({
      value: this.props.pathContext.wordpress_id,
      label: this.props.pathContext.name,
    })
  }
  render() {
    return (
      <div>
        <section className={'container'}>
          <Filter disable={'place'} />
          <Masonry gutter={20}>
            {this.props.data.allWordpressWpProjects !== null &&
              this.props.data.allWordpressWpProjects.edges
                .filter(({ node }) => filterProjects(node, this.props.filter))
                .map(({ node }) => (
                  <IndexProject
                    key={node.id}
                    slug={node.slug}
                    title={node.title}
                    img={node.featured_media.localFile.childImageSharp.sizes}
                    acf={node.acf}
                  />
                ))}
          </Masonry>
        </section>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  changePlace: value => {
    dispatch(changePlace(value))
  },
})

const mapStateToProps = state => {
  return { filter: state.filter }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaceFilterTemplate)

export const pageQuery = graphql`
  query placeFilterQuery($wordpress_id: Int!) {
    allWordpressWpProjects(filter: { _place: { eq: $wordpress_id } }) {
      edges {
        node {
          title
          id
          slug
          _function
          _place
          _status
          _year
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 680) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
          acf {
            description
          }
        }
      }
    }
  }
`
