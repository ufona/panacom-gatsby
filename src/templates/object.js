import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ArticleContent from '../components/ArticleContent'

const ObjectTemplate = ({ data, pathContext }) => {
  // const { locale } = pathContext
  // console.log(locale)
  // if (locale == 'en') {
  //   return (
  //     <ArticleContent
  //       title={data.wordpressWpProjects.wpml_translations[0].post_title}
  //       content={data.wordpressWpProjects.wpml_translations[0].post_content}
  //       acf={acf}
  //     />
  //   )
  // } else {
  return (
    <ArticleContent
      title={data.wordpressWpObjects.title}
      content={data.wordpressWpObjects.content}
      acf={data.wordpressWpObjects.acf}
      similar={pathContext.similar}
      object={true}
    />
  )
  // }
}
//<img src={post.image.sizes.thumbnail} />

ObjectTemplate.propTypes = {
  data: PropTypes.object.isRequired,
  edges: PropTypes.array,
}

export default ObjectTemplate

export const pageQuery = graphql`
  query currentWpObjectQuery($id: String!) {
    wordpressWpObjects(id: { eq: $id }) {
      title
      content
      wpml_translations {
        post_title
        post_content
      }
      acf {
        typeyear
        stages
        description
        header_image {
          source_url
        }
      }
    }
    site {
      siteMetadata {
        title
        subtitle
      }
    }
  }
`
