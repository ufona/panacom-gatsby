import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Masonry from 'react-masonry-component'
import IndexProject from '../components/ProjectCard'
import Filter from '../components/Filter'
import filterProjects from '../utils/filterProjects'
import Link from 'gatsby-link'
import { MoreButtonLink } from '../components/MoreButton'

const NavLink = props => {
  if (!props.test) {
    return <Link to={props.url}>{props.text}</Link>
  } else {
    return <span>{props.text}</span>
  }
}

const ProjectsTemplate = ({ data, filter, pathContext }) => {
  const { group, index, first, last, pageCount } = pathContext
  const previousUrl = index - 1 == 1 ? '' : (index - 1).toString()
  const nextUrl = (index + 1).toString()
  return (
    <div>
      <section className={'container'}>
        <Filter />
        <Masonry gutter={20}>
          {group.map(({ node }) => (
            <IndexProject
              key={node.id}
              slug={node.slug}
              title={node.title}
              img={node.featured_media.localFile.childImageSharp.sizes}
              acf={node.acf}
            />
          ))}
        </Masonry>
        {!last && <MoreButtonLink url={'/projects/' + nextUrl} />}
      </section>
    </div>
  )
}

ProjectsTemplate.propTypes = {
  data: PropTypes.object.isRequired,
}

const mapStateToProps = state => {
  return { filter: state.filter }
}

export default connect(mapStateToProps)(ProjectsTemplate)
