import React, { Component, Fragment } from 'react'
import Link from 'gatsby-link'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Masonry from 'react-masonry-component'
import MainSlider from '../components/MainSlider'
import IndexProject from '../components/ProjectCard'
import ArticleCard from '../components/ArticleCard'
import Filter from '../components/Filter'
import { MoreButton } from '../components/MoreButton'
import filterObjects from '../utils/filterObjects'
import { paginationIncrement, paginationReset } from '../actions/pagination'
import { initializeValues } from '../actions/filterObject'
import ObjectFilter from '../components/ObjectFilter'
class ObjectsTemplate extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    //this.props.resetPage()
  }

  render() {
    const publications = this.props.data.allWordpressWpPublications.edges
    let counter = 0
    const projects = this.props.data.allWordpressWpObjects.edges
    const projectsLength = projects.length
    const projectsPrint = projects
      .filter(({ node }) => filterObjects(node, this.props.filter))
      .slice(0, this.props.page)
      .map(({ node }, index) => {
        if ((index + 1) % 5 == 0) {
          let publication = publications[counter].node
          counter += 1
          return (
            <Fragment>
              <ArticleCard
                name={publication.title}
                caption={publication.content}
                link={publication.acf.link}
                type={publication.acf.type}
                date={publication.date}
                color={'yellow'}
                image={
                  publication.featured_media.localFile.childImageSharp.sizes
                }
              />
              <IndexProject
                key={node.id}
                slug={node.slug}
                title={node.title}
                img={node.featured_media.localFile.childImageSharp.sizes}
                acf={node.acf}
                func={node._function}
                type={'object'}
              />
            </Fragment>
          )
        } else {
          return (
            <IndexProject
              key={node.id}
              slug={node.slug}
              title={node.title}
              img={node.featured_media.localFile.childImageSharp.sizes}
              acf={node.acf}
              func={node._function}
              type={'object'}
            />
          )
        }
      })

    return (
      <div>
        <MainSlider />
        <ObjectFilter />
        <section className={'container projects'}>
          <Masonry gutter={20}>{projectsPrint.map(e => e)}</Masonry>
          {projectsLength - this.props.page >= 0 &&
            this.props.filter.selected == 97 && (
              <MoreButton onClick={() => this.props.nextPage()} />
            )}
        </section>
      </div>
    )
  }
}

ObjectsTemplate.propTypes = {
  data: PropTypes.object.isRequired,
  edges: PropTypes.array,
}

const mapDispatchToProps = dispatch => ({
  nextPage: () => {
    dispatch(paginationIncrement())
  },
  resetPage: () => {
    dispatch(paginationReset())
  },
})

const mapStateToProps = state => {
  return { filter: state.filterObject, page: state.pagination.postsToShow }
}

export default connect(mapStateToProps, mapDispatchToProps)(ObjectsTemplate)

export const pageQuery = graphql`
  query indexObjectQuery {
    allWordpressWpObjects {
      edges {
        node {
          title
          id
          slug
          design
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 680) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
          acf {
            description
          }
        }
      }
    }
    allWordpressWpPublications {
      edges {
        node {
          title
          id
          content
          date
          acf {
            type
            link
          }
          featured_media {
            localFile {
              childImageSharp {
                sizes(maxWidth: 230) {
                  ...GatsbyImageSharpSizes
                }
              }
            }
          }
        }
      }
    }
  }
`
