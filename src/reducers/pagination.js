import {
  PAGINATION_RESET,
  PAGINATION_INCREMENT,
} from '../constants/ActionTypes'

const initialState = {
  postsToShow: 20,
}

export default function pagination(state = initialState, action) {
  switch (action.type) {
    case PAGINATION_RESET:
      return {
        postsToShow: 20,
      }
    case PAGINATION_INCREMENT:
      return {
        postsToShow: state.postsToShow + 20,
      }
    default:
      return state
  }
}
