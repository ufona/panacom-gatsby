import { combineReducers } from 'redux'
import filterObject from './filterObjects'
import filter from './filter'
import pagination from './pagination'
export default combineReducers({ filter, pagination, filterObject })
