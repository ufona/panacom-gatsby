import {
  CHANGE_SLIDE,
  SLIDER_INIT,
  SLIDER_RESET,
} from '../constants/ActionTypes'

const initialState = {
  current: 0,
  items: [],
}

export default function pagination(state = initialState, action) {
  switch (action.type) {
    case CHANGE_SLIDE:
      return {
        ...state,
        current: current + 1,
      }
    case SLIDER_INIT:
      return {
        ...state,
        current: 0,
        items: [...action.payload],
      }
    case SLIDER_RESET:
      return {
        ...state,
        current: 0,
      }
    default:
      return state
  }
}
