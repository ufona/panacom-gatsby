import {
  CHANGE_PLACE,
  CHANGE_STATUS,
  CHANGE_YEAR,
  CHANGE_FUNCTION,
  FILTER_INIT,
  FILTER_RESET,
} from '../constants/ActionTypes'

const initialState = {
  selected: '',
  place: { value: 90, label: 'Места' },
  status: { value: 85, label: 'Статус' },
  year: { value: 89, label: 'Год' },
  function: { value: 86, label: 'Функция' },
  values: {
    places: [],
    statuses: [],
    years: [],
    functions: [],
  },
}

export default function filter(state = initialState, action) {
  switch (action.type) {
    case CHANGE_PLACE:
      return {
        ...state,
        selected: 'place',
        place: action.payload,
      }
    case CHANGE_STATUS:
      return {
        ...state,
        selected: 'status',
        status: action.payload,
      }
    case CHANGE_YEAR:
      return {
        ...state,
        selected: 'year',
        year: action.payload,
      }
    case CHANGE_FUNCTION:
      return {
        ...state,
        selected: 'function',
        function: action.payload,
      }
    case FILTER_INIT:
      return {
        ...state,
        values: action.payload,
      }
    case FILTER_RESET:
      return {
        ...state,
        selected: '',
        place: { value: 90, label: 'Места' },
        status: { value: 85, label: 'Статус' },
        year: { value: 89, label: 'Год' },
        function: { value: 86, label: 'Функция' },
      }
    default:
      return state
  }
}
