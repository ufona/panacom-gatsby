import {
  OBJECT_FILTER_CHANGE,
  OBJECT_FILTER_INIT,
  OBJECT_FILTER_RESET,
} from '../constants/ActionTypes'

const initialState = {
  selected: 97,
  values: [],
}

export default function filterObject(state = initialState, action) {
  switch (action.type) {
    case OBJECT_FILTER_CHANGE:
      return {
        ...state,
        selected: action.payload,
      }
    case OBJECT_FILTER_INIT:
      return {
        ...state,
        values: action.payload,
      }
    case OBJECT_FILTER_RESET:
      return {
        ...state,
        selected: 97,
      }
    default:
      return state
  }
}
